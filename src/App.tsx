import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { HomePage, PostsPage, AudioPage } from './components/Pages';
import AppHeader from './components/Header';
import './css/app.css';

class App extends React.Component {
  render() {
    return (
      <div className="app">
        <AppHeader />
        <div className="app__content">
          <Router>
            <Switch>
              <Route exact={true} path="/" component={HomePage} />
              <Route path="/posts" component={PostsPage} />
              <Route path="/audio" component={AudioPage} />
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
