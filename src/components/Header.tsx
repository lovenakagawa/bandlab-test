import * as React from 'react';

class AppHeader extends React.Component {
  render() {
    const logo = require('.././img/logo.png');

    return (
      <header className="app__header">
        <a className="btn--logo" href="/">
          <img src={logo} className="app__logo" alt="bandlab logo" />
        </a>
      </header>
    );
  }
}

export default AppHeader;
