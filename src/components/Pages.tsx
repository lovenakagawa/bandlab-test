import * as React from 'react';
import { Link } from 'react-router-dom';
import { fetchData } from '../services/FetchData';
import { IPostState, ISoundState } from '../interfaces/Interfaces';

const Sound = require('react-sound').default;
const spinner = require('.././img/spinner.gif');

export const HomePage = () => {
    return (
        <div className="container">
            <h1>Hi! I am a certified...</h1>
            <nav className="navigation">
                <Link to="/posts">Bibliophile</Link>
                <Link to="/audio">Audiophile</Link>
            </nav>
        </div>
    );
};

export class PostsPage extends React.Component<{}, IPostState> {
    constructor(props: {}, context: {}) {
        super(props, context);
        this.state = {
            posts: [],
            filteredPosts: [],
            isLoading: false,
            alphabets: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
            users: [],
            selectedUserId: 0,
        };
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        
        // Get Posts
        fetchData('posts').then(data => {
            this.setState({ 
                posts: data,
                filteredPosts: data,
                isLoading: false, 
            });
        });

        fetchData('users').then(data => {
            this.setState({
                users: data,
            });
        });
    }

    render () {
        return (
            <div className="container">
                <h1>Posts</h1>
                {this.showFilters()}
                {this.showUsers()}
                {this.showPosts()}
            </div>
        );
    }

    private showPosts = () => {
        const { filteredPosts, isLoading } = this.state;

        if (isLoading) {
            return <img src={spinner} />;
        }

        const sortedPosts = [...filteredPosts].sort((a, b) => {
            if (a.title < b.title) { 
                return -1;
            }
            if (a.title > b.title) { 
                return 1; 
            }
            return 0;
        });

        return (
            <ul className="posts">
                {
                    sortedPosts.map(post => 
                        <li className="post" key={post.id}>
                            <p className="post__title"><span>TITLE</span> {post.title}</p>
                        </li>
                    )
                }
            </ul>
        );

    }

    private handleFilter = (e: React.FormEvent<HTMLElement>) => {
        const value = e.target as HTMLElement;
        const filter = value.innerHTML;

        const { posts } = this.state;

        const filteredPosts = posts.filter(post => {
            return post.title[0] === filter.toLowerCase();
        });

        if (filteredPosts.length !== 0) {
            this.setState({ filteredPosts: filteredPosts });
            this.showPosts();
        } else {
            alert('Sorry no available posts for that filter');
        }
    }

    private handleChange = (e: React.FormEvent<HTMLSelectElement>) => {
        const user: string = e.currentTarget.value;

        const { posts } = this.state;

        const filteredPosts = posts.filter(post => {
            return post.userId === parseInt(user.substring(user.length - 1), 10);
        });

        if (filteredPosts.length !== 0) {
            this.setState({ filteredPosts: filteredPosts });
            this.showPosts();
        } else {
            alert('Sorry that user has no posts');
        }
    }

    private showFilters = () => {
        const { alphabets } = this.state;
        return (
            <ul className="filter">
                {
                    alphabets.map((letter, index) => 
                        <li key={index}><a onClick={this.handleFilter} >{letter}</a></li>
                    )
                }
            </ul>
        );
    }

    private showUsers = () => {
        const { users, selectedUserId } = this.state;
        return (
            <div className="users">
                <select value={selectedUserId} onChange={this.handleChange}>
                    {
                        users.map(user => 
                            <option key={user.id} value={user.id}>{user.name}</option>
                        )
                    }
                </select>
            </div>
        );
    }

}

export class AudioPage extends React.Component<{}, ISoundState> {
    constructor(props: {}, context: {}) {
        super(props, context);
        this.state = {
            sounds: [
                { isPlaying: false, audioId: 0, audioUrl: 'https://static.bandlab.com/soundbanks/previews/new-wave-kit.ogg' },
                { isPlaying: false, audioId: 1, audioUrl: 'https://static.bandlab.com/soundbanks/previews/synth-organ.ogg' }
            ],
        };
    }

    render() {
        const { sounds } = this.state;

        return (
            <div className="container">
                <h1>Audio</h1>
                <nav className="navigation navigation--audio">
                    <a id="0" onClick={this.handlePlayButton}>
                        Audio 1
                    </a>
                    <a id="1" onClick={this.handlePlayButton}>
                        Audio 2
                    </a>
                </nav>

                {
                    sounds.map(a => {
                        return <Sound key={a.audioId} url={a.audioUrl} playStatus={a.isPlaying ? Sound.status.PLAYING : Sound.status.STOPPED} />;
                    })
                }
            </div>
        );
    }

    private handlePlayButton = (e: React.FormEvent<HTMLElement>) => {
        e.preventDefault();
        const value = e.target as HTMLElement;
        const id = parseInt(value.id.substring(value.id.length - 1), 10);

        const { sounds } = this.state;
        sounds[id].isPlaying = !sounds[id].isPlaying;

        this.setState({
            sounds,
        });
    }
}
