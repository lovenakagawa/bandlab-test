export const fetchData = ( q: string, ) => {
    const API: string = 'https://jsonplaceholder.typicode.com/';

    return fetch(API + q)
        .then(response => { return response.json(); })
        .then(json =>  { return json; });
};
