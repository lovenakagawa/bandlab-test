export interface IPost {
    userId: number;
    id: number;
    title: string;
    body: string;
}

export interface IUsers {
    id: number;
    name: string;
    username: string;
    email: string;
    address: string[];
    phone: string;
    website: string;
    company: string[];
}

export interface IPostState {
    posts: IPost[];
    filteredPosts: IPost[];
    isLoading?: boolean;
    alphabets: string[];
    users: IUsers[];
    selectedUserId: number;
}

export interface ISound {
    isPlaying: boolean;
    audioId: number;
    audioUrl: string;
}

export interface ISoundState {
    sounds: ISound[];
}