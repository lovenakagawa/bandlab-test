# BandLab App

A simple app using react, webpack, typescript, es6 & sass. 

This project was bootstrapped with [TypeScript React Starter](https://github.com/Microsoft/TypeScript-React-Starter)
from [Create React App](https://github.com/facebookincubator/create-react-app).

##Installation 

```bash
    $ npm install 
```

##Usage

To use webpack dev server & watch,

```bash
    $ npm start
```

Listens at **http://localhost:3000**